import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-mobs-life',
  templateUrl: './mobs-life.component.html',
  styleUrls: ['./mobs-life.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class MobsLifeComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MobsLifeComponent } from './mobs-life.component';

describe('MobsLifeComponent', () => {
  let component: MobsLifeComponent;
  let fixture: ComponentFixture<MobsLifeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MobsLifeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MobsLifeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

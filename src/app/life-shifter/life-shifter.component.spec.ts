import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LifeShifterComponent } from './life-shifter.component';

describe('LifeShifterComponent', () => {
  let component: LifeShifterComponent;
  let fixture: ComponentFixture<LifeShifterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LifeShifterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LifeShifterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

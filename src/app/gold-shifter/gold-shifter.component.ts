import { Component, OnInit, SimpleChanges, Input, ViewEncapsulation, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { LocalStorageService } from 'angular-2-local-storage';

@Component({
  selector: 'app-gold-shifter',
  templateUrl: './gold-shifter.component.html',
  styleUrls: ['./gold-shifter.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class GoldShifterComponent implements OnInit {

  constructor(public dialog: MatDialog, private localStorageService: LocalStorageService) { }

  ngOnInit() {
    this.loadValues();
  }

  private loadValues() {
    var keys = this.localStorageService.keys();
    if (keys.indexOf('gold') != -1) {
      this._gold = this.localStorageService.get('gold');
    }
    if (keys.indexOf('silver') != -1) {
      this._silver = this.localStorageService.get('silver');
    }
    if (keys.indexOf('copper') != -1) {
      this._copper = this.localStorageService.get('copper');
    }
    console.log(keys);
  }

  private storeValues() {
    this.localStorageService.set('gold', this._gold);
    this.localStorageService.set('silver', this._silver);
    this.localStorageService.set('copper', this._copper);
  }

  moneyDown() {
    let dialogRef = this.dialog.open(MoneyDownDialog, {
      width: '250px'
    });
  }

  private _gold = 0;
  private _silver = 0;
  private _copper = 0;

  get gold(): number {
    return this._gold;
  }
  set gold(gold: number) {
    if (gold == -1) {
      this._gold = 0;
      this.moneyDown();
    } else {
      this._gold = gold;
    }
    this.storeValues();
  }

  get silver(): number {
    return this._silver;
  }
  set silver(silver: number) {
    if (silver == -1) {
      if (this.gold == 0) {
        this.moneyDown();
      } else {
        this.gold -= 1;
        this._silver = 9;
      }
    } else if (silver == 10) {
      this.gold += 1;
      this._silver = 0;
    } else {
      this._silver = silver;
    }
    this.storeValues();
  }

  get copper(): number {
    return this._copper;
  }
  set copper(copper: number) {
    if (copper == -1) {
      if (this.gold == 0 && this.silver == 0) {
        this.moneyDown();
      } else {
        this.silver -= 1;
        this._copper = 9;
      }
    } else if (copper == 10) {
      this.silver += 1;
      this._copper = 0;
    } else {
      this._copper = copper;
    } 
    this.storeValues();
  }

}


@Component({
  selector: 'money-down-dialog',
  templateUrl: 'money-down-dialog.html',
})
export class MoneyDownDialog {

  constructor(public dialogRef: MatDialogRef<MoneyDownDialog>) { }

  onOkClick(): void {
    this.dialogRef.close();
  }

  fullPathname = 'assets/images/moneydown.gif'
}
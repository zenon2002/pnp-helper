import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GoldShifterComponent } from './gold-shifter.component';

describe('GoldShifterComponent', () => {
  let component: GoldShifterComponent;
  let fixture: ComponentFixture<GoldShifterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GoldShifterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GoldShifterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

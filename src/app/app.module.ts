import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule, MatMenuModule, MatSidenavModule, MatIconModule, MatToolbarModule, MatListModule, MatDialogModule, MatCardModule } from '@angular/material';
import { LocalStorageModule } from 'angular-2-local-storage'


import { AppComponent } from './app.component';
import { AppRoutingModule } from './/app-routing.module';
import { GoldShifterComponent, MoneyDownDialog } from './gold-shifter/gold-shifter.component';
import { LifeShifterComponent } from './life-shifter/life-shifter.component';
import { MobsLifeComponent } from './mobs-life/mobs-life.component';


const routes: Routes = [
  { path: '', redirectTo: '/gold', pathMatch: 'full' },
  { path: 'gold', component: GoldShifterComponent },
  { path: 'life', component: LifeShifterComponent },
  { path: 'moblife', component: MobsLifeComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    GoldShifterComponent,
    LifeShifterComponent,
    MoneyDownDialog,
    MobsLifeComponent
  ],
  imports: [
    RouterModule.forRoot(routes),
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    MatButtonModule,
    MatMenuModule,
    MatSidenavModule,
    MatIconModule,
    MatToolbarModule,
    MatListModule,
    MatDialogModule,
    MatCardModule,
    LocalStorageModule.withConfig({
      prefix: 'pnp-helper',
      storageType: 'localStorage'
    })
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [MoneyDownDialog]
})
export class AppModule { }
